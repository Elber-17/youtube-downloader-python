from time import sleep
from random import randrange
from urllib.request import urlretrieve
from subprocess import PIPE

from ffmpy3 import FFmpeg
import pafy
from eyed3 import load

def delete_temp_files(audio_path):
	from os import remove

	remove(audio_path)
	
	return

def set_tags(audio_path):
	audiofile = load(audio_path)

	if not audiofile.tag:
		audiofile.initTag()

	audiofile.tag.artist = input('Ingresa el nombre del artista : ')
	audiofile.tag.album = input('Ingresa el nombre del albúm : ')
	# audiofile.tag.album_artist = 'dogtor'
	audiofile.tag.title = input('Ingresa el titulo de la canción : ')
	audiofile.tag.track_num = 1

	audiofile.tag.save(version=(1,None,None))
	audiofile.tag.save(version=(2,3,0))
	audiofile.tag.save()

	return True

def convert(audio_path, extension):
	executable = r'ffmpeg'
	input_file = {audio_path : None}

	if extension == 'webm':
		output_file = {audio_path[:-4] + 'mp3' : '-acodec libmp3lame -vol 288'}

	else:
		raise BaseException('Que mal programador eres >:c')


	ff = FFmpeg(executable=executable, inputs=input_file, outputs=output_file)

	return ff.run(stdout=PIPE, stderr=PIPE)

def download(audio, extension):
	from os.path import join
	audio_path = join('Musics', input('Ingresa el nombre del archivo a guardar : ') + '.' + extension)
	audio.download(audio_path)

	return audio_path

def show_audio_info(audio):
	print('Extensión : ', audio.extension)
	sleep(0.5)
	print('Peso : ', audio.get_filesize())

def get_audio(video):
	audio = video.getbestaudio()
	
	show_audio_info(audio)
	
	return audio

def show_video_info(video):
	print('\nTítulo : ', video.title)
	sleep(0.5)
	print('Duración : ', video.duration)
	sleep(0.5)
	print('Autor : ', video.author)
	print('Thumbnail : ', video.thumb)

def get_video():
	url = input('Ingrese la url del video : ')
	video = pafy.new(url)

	show_video_info(video)

	return video

def main():
	video = get_video()
	title = video.title
	audio = get_audio(video)
	extension = audio.extension
	audio_path = download(audio, extension)
	stdout, stderr = convert(audio_path, extension)
	tags = set_tags(audio_path[:-4] + 'mp3')
	
	delete_temp_files(audio_path)

	return

main()